source $HOME/.zshrc

# ~/.zshrc:
# ------------------------------------------------------------------------------
# export SYSNAME="name" # system-specific nickname

# export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
# source $ZDOTDIR/zshrc

# # - fzf: fuzzy file finder (type **<Tab> to use)
# if [ -f ~/.config/.fzf.zsh ]; then
#     export FZF_DEFAULT_COMMAND='ag --ignore .git -g ""' # requires ag
#     source $XDG_CONFIG_HOME/.fzf.zsh
# fi
