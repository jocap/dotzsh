# ==============================================================================
#                               zsh: configuration
# ==============================================================================

# These settings are only for interactive shells. Return if not interactive.
# This stops us from ever accidentally executing, rather than sourcing, .zshrc
[[ -o nointeractive ]] && return

function load {
    source $ZDOTDIR/load/$1.zsh
}

load autoload
load shellopts
load variables
load functions
load aliases
load options
load bindings
load helper
load history
load completion
load prompt
load colors
load externals
load fn/history/zsh-history-substring-search
load fn/history # zsh-history-substring-search configuration

# System-specific loads
# ($SYSNAME should be set in ~/.zshrc)
if [[ $SYSNAME = 'WSL' ]]; then
    # Windows Subsystem for Linux
    load system/wsl
elif [[ $SYSNAME = 'Webfaction' ]]; then
	# Webfaction server
    load system/webfaction
elif [[ $SYSNAME = 'linuxvm' ]]; then
    # Linux virtual machine
    load system/linuxvm
fi
