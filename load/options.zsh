# (Borrowed from oh-my-zsh)

#  cd adds directories to the stack like pushd:
setopt AutoPushd           2>/dev/null

#  the same folder will never get pushed twice:
setopt PushdIgnoreDups     2>/dev/null

#  - and + are reversed after cd:
setopt PushdMinus          2>/dev/null

#  pushd will not print the directory stack after each invocation:
setopt PushdSilent         2>/dev/null

#  pushd with no parameters acts like 'pushd $HOME':
setopt PushdToHome         2>/dev/null

#  Allow short forms of function contructs:
setopt CompleteAliases     2>/dev/null

#  Allow comments in an interactive shell.:
setopt InteractiveComments 2>/dev/null

#  Tab on ambiguous completions cycles through possibilities:
setopt AutoMenu            2>/dev/null

#  Complete Mafile to Makefile if cursor is on the f:
setopt CompleteInWord      2>/dev/null

#  Attempt to spell-check command names - I mistype a lot:
setopt Correct             2>/dev/null

#  Allow variable substitution in prompt:
setopt prompt_subst        2>/dev/null
