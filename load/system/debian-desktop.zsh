# Path

local ORIG_PATH="$PATH"

export PATH="$HOME/dot/bin:$HOME/.local/bin"
export PATH="$PATH:$ORIG_PATH"
