local LAUNCH_PROGRAM=0
local LAUNCH_PROGRAM="$LAUNCH_AT_START"
export LAUNCH_AT_START=0 # prevent launched program to launch more programs

case "$LAUNCH_PROGRAM" in
    emacs)
        nohup emacs &> /dev/null & disown;;
    gnome-terminal)
        nohup gnome-terminal &> /dev/null & disown;;
esac
