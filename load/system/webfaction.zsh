# NB: Not all of this is necessary for Webfaction.
# TODO: Check what paths are needed

export PATH=~/bin # custom installs
export PATH=$PATH:~/.rbenv/bin # custom installs
export PATH=$PATH:/usr/bin:/bin:/usr/sbin:/usr/local/bin:/opt/local/bin:/lib # general
export PATH=$PATH:/usr/local/mysql/bin:/usr/local/git/bin
export PATH=$PATH:~/local/bin
