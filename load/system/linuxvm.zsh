# Path

export PATH=~/bin
export PATH=$PATH:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games

# X11 configuration

# Check whether config is already loaded (might need updating in the future)
if [[ "$DISPLAY" != "" ]] && [[ $(xmodmap | grep -c "control     Control_R (0x24),  Control_L (0x25),  Control_L (0x42),  Control_R (0x6d)") -eq 0 ]]; then
    echo "Configuring X11 ..."
    ~/.xinitrc
    if [[ $? -eq 0 ]]; then
        echo "DONE"
    else
        echo "FAILED"
    fi
fi

# Shared folders (see ~/bin/remount script)

function remount {
    if [[ $(stat --format "%U" /mnt/hgfs/) == "root" ]]; then
        local dir=$(pwd)
        local realdir=$(realpath `pwd`)
        [[ "$realdir" == /mnt/hgfs/* ]] && \cd # get out of mounted drive

        echo "Remounting shared folders at /mnt/hgfs/ ..."
        sudo umount /mnt/hgfs
        if [[ $? -eq 0 ]]; then
            vmhgfs-fuse .host:/ /mnt/hgfs -o uid=`id -u` -o gid=`id -g` -o umask=0033
        fi

        if [[ $(stat --format "%U" /mnt/hgfs/) == "root" ]]; then
            echo "FAILED"
        else
            echo "DONE"
        fi

        \cd "$dir" # return to directory
    fi
}

remount
