# WSL-specific configuration

# Path
local WSL_PATH=$PATH
export PATH=$HOME/bin
export PATH=$PATH:$HOME/local/bin
export PATH=$PATH:$HOME/.local/bin
export PATH=$PATH:$WSL_PATH # includes Windows programs

# Man path
MANPATH=$HOME/.fzf/man/:$MANPATH; export MANPATH

# Libraries
export LD_LIBRARY_PATH=~/local/lib
export PKG_CONFIG_PATH=~/local/lib/pkgconfig

# Aliases
if command -v pandoc > /dev/null 2>&1; then
    alias pandoc='pandoc +RTS -V0 -RTS' # w/o timer_create, b/c it doesn't exist on this WSL
fi

# X server
export DISPLAY=:0

# LaTeX configuration
export TEXMFHOME=$HOME/.texmf

# Disable background process niceness (because it doesn't work in latest WSL)
unsetopt BG_NICE

# Fix directory permissions
umask 022

# Keyboard configuration
if command -v xmodmap > /dev/null 2>&1; then
	if [[ $(xmodmap | grep -c '^mod3 *Super_L.*$') -eq 0 ]]; then
	    echo "Configuring xmodmap ..."
	    xmodmap ~/.Xmodmap
	    if command -v xcape > /dev/null 2>&1; then
                xcape -e 'Control_R=Return'
            fi
	fi
fi

load system/wsl/launch_at_start
