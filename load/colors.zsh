# Custom terminal colors
if [[ -f $XDG_CONFIG_HOME/.dircolors ]]; then
    eval $(dircolors -b $XDG_CONFIG_HOME/.dircolors)
fi
