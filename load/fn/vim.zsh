# Function to start and interact with vim/nvim server
# - For vim support, vim needs to be compiled with +clientserver
# - For nvim support, neovim-remote is required

alias nvm="vm -n" # NOTE: might conflict with Node Version Manager
function vm {
    # Default settings
    local vim='vim'                    # vim/nvim
    local file=$@[-1]                  # file to be opened (last argument)
    local running=false                # whether server is running
    local method='split'               # default opening method
    local session=false                # whether $file is a session file
    local nvim_serverdir='/tmp'        # nvim server directory
    local nvim_servername='nvimsocket' # nvim server file name -> default: /tmp/nvimsocket
    local vim_servername='VIM'         # vim server file name

    # User-set options (${+var} returns true if $var is set)
    (( ${+vm_default_method} )) && local method=$vm_default_method
    (( ${+vm_nvim_serverdir} )) && local nvim_serverdir=$vm_nvim_serverdir
    (( ${+vm_nvim_servername} )) && local nvim_servername=$vm_nvim_servername
    (( ${+vm_vim_servername} )) && local vim_servername=$vm_vim_servername

    # Handle command-line arguments
    for arg; do
        case "$arg" in
            -b)
                local method='buffer';;
            -t)
                local method='tab';;
            -v)
                local method='vsplit';;
            -s)
                local method='split';;
            -S) # open stored session file (like vim -S)
                local session=true;;
            -n) # use nvim instead of vim
                local vim='nvim';;
        esac
    done

    # Check if server is already running
    if [ $vim = 'vim' ]; then
        [[ $(eval 'vim --serverlist | grep -i -c "^$vim_servername\$"') -ne 0 ]] && local running=true
    fi
    if [ $vim = 'nvim' ]; then
        [[ -e $(eval echo "$nvim_serverdir/$nvim_servername") ]] && local running=true
        # ^ remember to use -e instead of -f (-f is only true if file is normal)
    fi

    local filepath=$file # path to file

    # If file exists, get the full path
    if [[ -f "$file" || -d "$file" ]]; then
        local filepath=$(readlink -f "$file")
    fi

    if [ $running = true ] && [ $file != '' ]; then

        if [ $vim = 'nvim' ]; then # nvim
            local option='' # default: same buffer
            case $method in
                (split)
                    local option='-o'
                    ;;
                (vsplit)
                    local option='-O'
                    ;;
                (tab)
                    local option='--remote-tab'
                    ;;
                (buffer)
                    local option=''
                    ;;
            esac
            eval "nvr --servername $nvim_serverdir/$nvim_servername $option '$filepath'" 2> /dev/null
        else # vim
            local option="--remote '$filepath'" # default: same buffer
            case $method in
                (split)
                    local option="--remote-send ':split $filepath<CR>'"
                    ;;
                (vsplit)
                    local option="--remote-send ':vsplit$filepath<CR>'"
                    ;;
                (tab)
                    local option="--remote-tab '$filepath'"
                    ;;
                (buffer)
                    local option="--remote '$filepath'"
                    ;;
            esac
            eval "vim --servername $vim_servername $option"
        fi
    else
        if ! [ -z "$file" ]; then # not empty
            file="'$file'"
            [ $session = true ] && file="-S $file"
        fi

        if [ $vim = 'nvim' ]; then
            # Launch nvim:
            eval "NVIM_LISTEN_ADDRESS=$nvim_serverdir/$nvim_servername nvim $file" # launch nvim as server

            # Make sure server file is removed when server is closed:
            if [[ -e $(eval echo "$nvim_serverdir/$nvim_servername") ]]; then
                "$nvim_serverdir/$nvim_servername" 
            fi
        else
            # Launch vim:
            eval "vim --servername $vim_servername $file"
        fi
    fi
}
