function hg_prompt_info {
    hg prompt --angle-brackets "%{$PR_WHITE%}<#<bookmark> >%{$PR_BLUE%}[<branch>%{$PR_GREEN%}<status|modified|unknown><update>%{$PR_BLUE%}]%{$PR_NO_COLOR%}" 2>/dev/null
}
