# Function to interact with emacs server

function em {
    # Default settings
    local file=$@[-1]

    # If file exists, get the full path
    if [[ -f "$file" || -d "$file" ]]; then
        local filepath=$(readlink -f "$file")
    else
        local filepath="$file"
    fi

    local command='(find-file "'$filepath'")'

    case $1 in
        -b) # buffer
            emacsclient -e "$command";;
        -s) # (h)split
            emacsclient -e '(select-window (split-window-below))' -e "$command";;
        -v) # vsplit
            emacsclient -e '(select-window (split-window-right))' -e "$command";;
        -f) # frame
            emacsclient -e '(select-frame (new-frame))' -e "$command";;
        -t) # tty
            echo -ne "\e[6 q"
            emacsclient -t "$filepath"
            echo -ne "\e[2 q";;
        -k) # kill
            emacsclient -t -e '(save-buffers-kill-emacs)';;
        -d) # daemon
            emacs --daemon;;
        -r) # restart
            echo -ne "\e[6 q"
            emacsclient -t -e '(save-buffers-kill-emacs)'; emacs --daemon &> /dev/null
            echo -ne "\e[2 q";;
        -e) # execute command

            emacsclient -e "$*";;
        *) # default
            emacsclient -e "$command";;
    esac

}
