# New `cd` function

function d {
    cd $*
    ls -G --color=auto --ignore='NTUSER.*' --ignore='ntuser.*'
}
alias cd="d"
