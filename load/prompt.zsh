typeset +x PS1
setopt promptsubst

# Counts files in current directory. If there are no files, -1 is printed.
function dir-files {
    echo -n "%F{yellow}"
    echo -n "$(($(ls -l | wc -l) - 1))"
    echo -n "%f" # reset
}

ZSH_THEME_GIT_PROMPT_PREFIX='%F{blue}['
ZSH_THEME_GIT_PROMPT_SUFFIX='%F{blue}]%f'
ZSH_THEME_GIT_PROMPT_DIRTY='%F{green}!'
ZSH_THEME_GIT_PROMPT_UNTRACKED='%F{green}?'
ZSH_THEME_GIT_PROMPT_CLEAN=''

PROMPT='%F{yellow}%n %F{grey}%(5~|%-1~/…/%3~|%4~) $(dir-files) → '
# truncated working directory ^^^^^^^^^^^^^^^^^^^^
RPROMPT='$(git_prompt_info) %F{yellow}%D{%H:%M} %F{green}%D{%d}%f'
# hg_prompt_info - too slow!
