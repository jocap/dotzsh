alias ..="cd .."
alias ...="cd ../.."
alias pu="pushd"
alias po="popd"
alias cl="clear;ls -G"
alias ls="ls -G --color=auto --ignore='NTUSER.*' --ignore='ntuser.*'"
alias grep="grep --color=auto"

alias sudo="sudo "
# ^ makes using aliases with sudo work
# ^ (an alias ending in a space allows the next word to be alias expanded)

alias hs="hg status"
alias gs="git status"

alias vz="nvm ~/.zsh"
alias vv="nvm ~/.vim/vimrc"
