# Shell

export TERM=xterm-256color
export SHELL=$(whence -p zsh)           # Let apps know the full path to zsh
export DIRSTACKSIZE=10                  # Max number of dirs on the dir stack
export TMPPREFIX=$HOME/.zsh/tmp

# Settings

export EDITOR="vim"
export BROWSER="firefox"
export XDG_CONFIG_HOME=$HOME/.config    # Probably won't change between systems
export LANG=en_US.UTF-8

export LESSHISTFILE=/dev/null

export vm_default_method='vsplit'       # default open method for vm() (see fn/vim.zsh)
export vm_nvim_serverdir='~/.servers'   # default server dir for vm()
export vm_nvim_servername='nvim'        # default server name for vm()
