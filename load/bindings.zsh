bindkey -e                              # Use emacs keybindings
#bindkey -v                             # Use vi keybindings

bindkey "\e[1~"   beginning-of-line     # Another Home
bindkey "\e[4~"   end-of-line           # Another End
bindkey "\e[3~"   delete-char           # Another Delete

# Have <C-backspace> stop at /, but C-w skip /
# (courtesy of JunkMechanic @ Unix StackExchange)
backward-kill-dir () {
    local WORDCHARS=${WORDCHARS/\/}
    zle backward-kill-word
}
zle -N backward-kill-dir
bindkey '^[^?' backward-kill-dir
